#ifndef TRUTH_SELECTOR_CONFIG_HH
#define TRUTH_SELECTOR_CONFIG_HH

#include <string>
#include <vector>


struct TruthKinematicConfig
{
  float pt_minimum      = 2e3;
  float abs_eta_maximum = 2.5;
  float dr_maximum      = 0.4;
};

struct TruthSelectorConfig
{
  std::vector<std::string> containers;
  enum class Particle { hadron, lepton, fromBC, overlapLepton,
    promptLepton, nonPromptLepton, muon, stableNonGeant };
  Particle particle;
  TruthKinematicConfig kinematics;
};

#endif

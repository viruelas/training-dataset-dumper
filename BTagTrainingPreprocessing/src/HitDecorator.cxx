#include "HitDecorator.hh"
#include "HitDecoratorConfig.hh"

#include "GeoPrimitives/GeoPrimitives.h"
#include "xAODJet/Jet.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/Vertex.h"
#include <vector>
#include <numeric>
#include <cmath>

namespace {

  // couple functions to get 3 vectors from various places
  Amg::Vector3D get3Vector(const xAOD::TrackMeasurementValidation& h) {
    return {h.globalX(), h.globalY(), h.globalZ()};
  }

  Amg::Vector3D getShifted3Vector(
    const xAOD::TrackMeasurementValidation& h,
    const xAOD::Vertex& v) {
    Amg::Vector3D p = v.position();
    return get3Vector(h) - p;
  }
  float multJump(const int N1, const int N2) { // definition for a multiplicity jump, N1, N2, ... are used as placeholders here
   return float(N2 - N1)/float(N2+1);
   // avoid the case of dividing by zero by adding a 1 in the denominator
  } 

  float avg_distance_hit_jet_axis(const std::vector<float>& drs) { // definition for calculating the average distance from a hit to jet axis
  float sum = std::accumulate(drs.begin(), drs.end(), 0.0f); // sum up the delta_R values
  int size = drs.size(); // calculate the size of the incoming vector
  if (size != 0) return sum/size; // calculate the average
  else return NAN; // use Not a Number when there are no hits apparent
  }

  float inv_total_hits_def(const int N6, const int N7, const int N8, const int N9) { // definition for the inverse sum of the number of hits in all layers
   return float(1.0)/float(N6+N7+N8+N9+1);
   // avoid the case of dividing by zero by adding a 1 in the denominator
  }


}


// you have to give the decorators their names here
HitDecorator::HitDecorator(const HitDecoratorConfig& cfg):
  m_avg_hit_jet_0("hits_averageDeltaRToJetAxisL0"),
  m_avg_hit_jet_1("hits_averageDeltaRToJetAxisL1"),
  m_avg_hit_jet_2("hits_averageDeltaRToJetAxisL2"),
  m_avg_hit_jet_3("hits_averageDeltaRToJetAxisL3"),
  m_SOAP("hits_SOAP"),
  m_multiJump03("hits_multiJump03"),
  m_nHits_L0("hits_nL0"),
  m_nHits_L1("hits_nL1"),
  m_nHits_L2("hits_nL2"),
  m_nHits_L3("hits_nL3"),
  m_total_hits("hits_sum"),
  m_inv_sum("hits_inversSum"),
  m_dR_hit_to_jet(cfg.dR_hit_to_jet),
  m_save_endcap_hits(cfg.save_endcap_hits),
  m_use_splitProbability(cfg.use_splitProbability),
  m_layer("layer"),
  m_bec("bec"),
  m_splitProbability1("splitProbability1"),
  m_splitProbability2("splitProbability1")
{
}

// the main function you'll call
void HitDecorator::decorate(
  const xAOD::Jet& jet,
  const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
  const xAOD::Vertex& vertex) const
{
  std::vector<float> deltaR_0; // define a vector deltaR_N where the deltaR between hit and jet axis is saved in layer N
  std::vector<float> deltaR_1; 
  std::vector<float> deltaR_2; 
  std::vector<float> deltaR_3; 
  
  // loop over all the hits and save the ones that are within some
  // radius
  int nHitsL0(0), nHitsL1(0), nHitsL2(0), nHitsL3(0);
  for (const auto* raw_hit: hits) {
    // Do not count the hit if it's in EC and we only want barrel hits
    int isEC = (abs(m_bec(*raw_hit)) == 2);
    if (isEC && !m_save_endcap_hits) continue;
    
    // Use splitProbability to determine how many charged particles
    // created this cluster - values correspond to Run2, taken from InDetConfigFlags.py
    // The multiplicity assignment follows InDetDenseEnvAmbiTrackSelectionTool::isTwoPartClus/isMultiPartClus
    int mult = 1;
    if (m_use_splitProbability) {
      float sp1 = m_splitProbability1(*raw_hit);
      float sp2 = m_splitProbability2(*raw_hit);
      if (sp2 > 0.45) mult = 3;
      else {
        if (sp1 > 0.55) mult = 2;
      }
    }
    
    // Count hits within a given dR
    // pay attention to count the hits and deltaR values according to a split hit (as defined above in m_use_splitProbability)
    TVector3 local(getShifted3Vector(*raw_hit, vertex).data());
    if (jet.p4().Vect().DeltaR(local) < m_dR_hit_to_jet) {  
      int layer = m_layer(*raw_hit);
      if (layer == 0)  {
        nHitsL0 += mult;
        deltaR_0.insert(deltaR_0.end(), mult, jet.p4().Vect().DeltaR(local));
      }
      else if (layer == 1) {
        nHitsL1 += mult;
        deltaR_1.insert(deltaR_1.end(), mult, jet.p4().Vect().DeltaR(local));
      }
      else if (layer == 2) {
        nHitsL2 += mult;
        deltaR_2.insert(deltaR_2.end(), mult, jet.p4().Vect().DeltaR(local));
      }
      else if (layer == 3) {
        nHitsL3 += mult;
        deltaR_3.insert(deltaR_3.end(), mult, jet.p4().Vect().DeltaR(local));
      };
    }
  }


  // decorate the jet with something
  // use the avg_distance_hit_jet_axis with deltaR_N as an input to calculate the average distance of hist to the jet axis
  
  m_avg_hit_jet_0(jet) = avg_distance_hit_jet_axis(deltaR_0);
  m_avg_hit_jet_1(jet) = avg_distance_hit_jet_axis(deltaR_1);
  m_avg_hit_jet_2(jet) = avg_distance_hit_jet_axis(deltaR_2);
  m_avg_hit_jet_3(jet) = avg_distance_hit_jet_axis(deltaR_3);
  m_SOAP(jet) = multJump(nHitsL0,nHitsL3)+multJump(nHitsL0,nHitsL2)+multJump(nHitsL0,nHitsL1)+multJump(nHitsL1,nHitsL3)+multJump(nHitsL1,nHitsL2)+multJump(nHitsL2,nHitsL3);
  m_multiJump03(jet) = multJump(nHitsL0,nHitsL3);
  m_nHits_L0(jet) = nHitsL0;
  m_nHits_L1(jet) = nHitsL1;
  m_nHits_L2(jet) = nHitsL2;
  m_nHits_L3(jet) = nHitsL3;
  m_total_hits(jet) = nHitsL0+nHitsL1+nHitsL2+nHitsL3;
  m_inv_sum(jet) = inv_total_hits_def(nHitsL0,nHitsL1,nHitsL2,nHitsL3);
  
}

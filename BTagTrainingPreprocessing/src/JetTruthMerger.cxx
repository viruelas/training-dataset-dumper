#include "JetTruthMerger.hh"

#include "xAODJet/Jet.h"

// the constructor just builds the decorator
JetTruthMerger::JetTruthMerger(const std::vector<std::string>& incoming,
                               const std::string& outgoing):
  m_deco(outgoing)
{
  for (const std::string& name: incoming) {
    m_acc.emplace_back(name);
  }
}

// this call actually does the work on the jet
void JetTruthMerger::decorate(const xAOD::Jet& jet) const
{
  PartLinks out_links;
  for (const SG::AuxElement::ConstAccessor<PartLinks>& link_acc: m_acc) {
    const PartLinks& links = link_acc(jet);
    out_links.insert(out_links.end(), links.begin(), links.end());
  }
  m_deco(jet) = out_links;
}


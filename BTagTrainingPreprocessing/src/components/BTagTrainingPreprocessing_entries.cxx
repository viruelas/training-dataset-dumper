#include "src/SingleBTagAlg.h"
#include "src/TrackSystematicsAlg.h"
#include "src/TriggerJetGetterAlg.h"
#include "src/TriggerBTagMatcherAlg.h"
#include "src/JetMatcherAlg.h"
#include "src/MCTCDecoratorAlg.h"

#include "src/H5FileSvc.h"

DECLARE_COMPONENT(SingleBTagAlg)
DECLARE_COMPONENT(TrackSystematicsAlg)
DECLARE_COMPONENT(TriggerJetGetterAlg)
DECLARE_COMPONENT(TriggerBTagMatcherAlg)
DECLARE_COMPONENT(JetMatcherAlg)
DECLARE_COMPONENT(MCTCDecoratorAlg)

DECLARE_COMPONENT(H5FileSvc)

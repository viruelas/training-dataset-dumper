#include "SingleBTagAlg.h"

#include "SingleBTagTools.hh"
#include "SingleBTagConfig.hh"
#include "processSingleBTagEvent.hh"
#include "ConfigFileTools.hh"

#include "H5Cpp.h"

#include "StoreGate/ReadHandle.h"

#include <nlohmann/json.hpp>
#include <filesystem>

SingleBTagAlg::SingleBTagAlg(const std::string& name,
                             ISvcLocator* pSvcLocator):
  AthAlgorithm(name, pSvcLocator),
  m_tools(nullptr),
  m_outputs(nullptr)
{
  declareProperty("configFileName", m_config_file_name);
  declareProperty("metadataFileName",
                  m_metadata_file_name = "userJobMetadata.json");
}

SingleBTagAlg::~SingleBTagAlg() {
}

  // these are the functions inherited from Algorithm
StatusCode SingleBTagAlg::initialize () {
  namespace fs = std::filesystem;
  fs::path cfg_path(m_config_file_name);
  if (!fs::exists(cfg_path)) {
    ATH_MSG_ERROR(cfg_path.string() << " doesn't exist");
    return StatusCode::FAILURE;
  }
  std::ifstream cfg_stream(cfg_path);
  auto nlocfg = nlohmann::ordered_json::parse(cfg_stream);
  std::string prefix = cfg_path.stem();
  ConfigFileTools::combine_files(nlocfg, cfg_path.parent_path());
  if (!m_h5_dir.empty()) {
    nlocfg = nlocfg.at(m_h5_dir).at("configuration");
    prefix = m_h5_dir;
  }

  auto cfg = get_singlebtag_config(nlocfg, prefix);
  if (m_force_full_precision) force_full_precision(cfg);
  m_config.reset(new SingleBTagConfig(cfg));
  m_tools.reset(new SingleBTagTools(*m_config));

  ATH_CHECK(m_output_svc.retrieve());
  if (m_h5_dir.empty()) {
    m_outputs.reset(new SingleBTagOutputs(*m_config, *m_output_svc->file()));
  } else {
    H5::Group group(m_output_svc->file()->createGroup(m_h5_dir));
    m_outputs.reset(new SingleBTagOutputs(*m_config, group));
  }

  m_jetKey = cfg.jet_collection;
  ATH_CHECK(m_jetKey.initialize());
  ATH_MSG_DEBUG("Initialized " << name());

  return StatusCode::SUCCESS;
}
StatusCode SingleBTagAlg::execute () {

  // dereference a few things for convenience
  auto& event = *evtStore();
  const auto& jobcfg = *m_config;
  auto& tools = *m_tools;
  auto& outputs = *m_outputs;

  // most of the real work happens in this function
  processSingleBTagEvent(event, jobcfg, tools, outputs);

  return StatusCode::SUCCESS;
}
StatusCode SingleBTagAlg::finalize () {

  if (m_metadata_file_name.size() > 0) {
    writeTruthCorruptionCounts(m_outputs->truth_counts, m_metadata_file_name);
  }

  return StatusCode::SUCCESS;
}

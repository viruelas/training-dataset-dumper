#include "SingleBTagConfig.hh"
#include "ConfigFileTools.hh"

#define BOOST_BIND_GLOBAL_PLACEHOLDERS // ignore deprecated ptree issues
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/optional/optional.hpp>

#include <filesystem>
#include <set>

// set up some nlohmann converters from json, used by nlohmann::get<T>()
NLOHMANN_JSON_SERIALIZE_ENUM( DL2Config::Where, {
    {DL2Config::Where::UNKNOWN, ""},
    {DL2Config::Where::JET, "jet"},
    {DL2Config::Where::BTAG, "btag"},
})
NLOHMANN_JSON_SERIALIZE_ENUM( DL2Config::Engine, {
    {DL2Config::Engine::UNKNOWN, ""},
    {DL2Config::Engine::DL2, "dl2"},
    {DL2Config::Engine::GNN, "gnn"},
})

namespace {

  const std::string g_ip_prefix = "ip_prefix";

  TrackSortOrder get_track_sort_order(const boost::property_tree::ptree& pt,
                                const std::string& key) {
    std::string val = pt.get<std::string>(key);
    if (val == "abs_d0_significance") {
      return TrackSortOrder::ABS_D0_SIGNIFICANCE;
    }
    if (val == "abs_d0") {
      return TrackSortOrder::ABS_D0;
    }
    if (val == "d0_significance") {
      return TrackSortOrder::D0_SIGNIFICANCE;
    }
    if (val == "abs_beamspot_d0") {
      return TrackSortOrder::ABS_BEAMSPOT_D0;
    }
    if (val == "pt") {
      return TrackSortOrder::PT;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }

  std::vector<JetConstituentWriterConfig::Output> get_constituent_variables(
    const boost::property_tree::ptree& pt)
  {
    using Type = JetConstituentWriterConfig::Type;
    using Op = JetConstituentWriterConfig::Output;
    namespace cf = ConfigFileTools;
    std::vector<JetConstituentWriterConfig::Output> vars;
    std::set<std::string> keys = cf::get_key_set(pt);
    auto fill = [&vars, &keys, &pt](const std::string& name, Type type) {
      if (keys.erase(name)) {
        for (auto v: cf::get_list(pt.get_child(name))) {
          vars.emplace_back<Op>({type, v});
        }
      }
    };
    fill("uchars", Type::UCHAR);
    fill("chars", Type::CHAR);
    fill("ints", Type::INT);
    fill("halves", Type::HALF);
    fill("floats", Type::FLOAT);
    fill("customs", Type::CUSTOM);
    fill("half_precision_customs", Type::CUSTOM_HALF);
    cf::throw_if_any_keys_left(keys, "track variable types");
    return vars;
  }

  auto get_constituent_outblock_config(const boost::property_tree::ptree& pt)
  {
    const std::string edm_block_name = "edm_names";
    JetConstituentWriterConfig::OutputBlock cfg;
    cfg.outputs = get_constituent_variables(pt.get_child("variables"));
    if (pt.count(edm_block_name)) {
      for (const auto& [outname, edmname]: pt.get_child("edm_names")) {
        cfg.edm_name[outname] = edmname.data();
      }
    }
    cfg.allow_unused_edm_names = false;
    if (auto v = pt.get_optional<bool>("allow_unused_edm_names")) {
      cfg.allow_unused_edm_names = *v;
    }
    return cfg;
  }

  auto get_constituent_config(const boost::property_tree::ptree& pt) {
    const std::string association_block = "associations";
    JetConstituentWriterConfig writer;
    writer.constituent = get_constituent_outblock_config(pt);
    writer.name = pt.get<std::string>("output_name");
    writer.size = pt.get<size_t>("n_to_save");
    if (pt.count(association_block)) {
      for (const auto& [type, block]: pt.get_child(association_block)) {
        writer.associated[type] = get_constituent_outblock_config(block);
      }
    }
    return writer;
  }

  auto get_jet_link_config(const boost::property_tree::ptree& pt) {
    JetLinkWriterConfig cfg;
    cfg.constituents = get_constituent_config(pt);
    cfg.accessor = "constituentLinks";
    return cfg;
  }

  TrackConfig get_track_config(const boost::property_tree::ptree& pt) {
    namespace cft = ConfigFileTools;
    TrackConfig cfg;

    cfg.sort_order = get_track_sort_order(pt,"sort_order");

    const boost::property_tree::ptree& tracksel = pt.get_child("selection");
    TrackSelectorConfig::Cuts& cuts = cfg.selection.cuts;
    cuts.pt_minimum        = cft::null_as_nan(tracksel,"pt_minimum");
    cuts.abs_eta_maximum   = cft::null_as_nan(tracksel,"abs_eta_maximum");
    cuts.d0_maximum        = cft::null_as_nan(tracksel,"d0_maximum");
    cuts.z0_maximum        = cft::null_as_nan(tracksel,"z0_maximum");
    cuts.si_hits_minimum   = tracksel.get<int>("si_hits_minimum");
    cuts.si_shared_maximum = tracksel.get<int>("si_shared_maximum");
    cuts.si_holes_maximum  = tracksel.get<int>("si_holes_maximum");
    cuts.pix_holes_maximum = tracksel.get<int>("pix_holes_maximum");

    cfg.selection.btagging_link = pt.get<std::string>("btagging_link");

    cfg.writer = get_constituent_config(pt);
    cfg.input_name = pt.get<std::string>("input_name");

    // use the same prefix for selection and everything else
    std::string ip_prefix = pt.get<std::string>(g_ip_prefix);
    if (cfg.writer.constituent.edm_name.count(g_ip_prefix)) {
      throw std::runtime_error(
        "you should not specify '" + g_ip_prefix
        + "' in track edm remapping");
    }
    cfg.writer.constituent.edm_name[g_ip_prefix] = ip_prefix;
    cfg.selection.ip_prefix = ip_prefix;

    return cfg;
  }

  HitConfig get_hit_config(const boost::property_tree::ptree& pt) {
    namespace cft = ConfigFileTools;
    HitWriterConfig writer;

    writer.output_size = pt.get<size_t>("output_size");
    writer.name = pt.get<std::string>("output_name");
    writer.dR_to_jet = pt.get<float>("dR_to_jet");
    writer.save_endcap_hits = pt.get<bool>("save_endcap_hits");
    writer.save_only_clean_hits = pt.get<bool>("save_only_clean_hits");

    HitDecoratorConfig decorator;
    decorator.dR_hit_to_jet = writer.dR_to_jet;
    decorator.use_splitProbability = pt.get<bool>("use_splitProbability");
    decorator.save_endcap_hits = writer.save_endcap_hits;

    return {writer, decorator};
  }

  TrackSortOrder get_truth_sort_order(const std::string& val) {
    if (val == "pt") {
      return TrackSortOrder::PT;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }

  TruthSelectorConfig::Particle get_truth_particle(const std::string& name)
  {
    using p = TruthSelectorConfig::Particle;
    if (name == "truth_hadrons") return p::hadron;
    if (name == "truth_leptons") return p::lepton;
    if (name == "truth_fromBC") return p::fromBC;
#define TRY(KEY) if (name == #KEY) return p::KEY
    TRY(overlapLepton);
    TRY(promptLepton);
    TRY(nonPromptLepton);
    TRY(muon);
    TRY(stableNonGeant);
#undef TRY
    throw std::logic_error("unknown truth particle type: " + name);
  }

  TruthConfig get_truth_config(const nlohmann::ordered_json& js) {
    TruthConfig cfg;

    using Opts = ConfigFileTools::OptionalConfigurationObject;
    Opts json(js);

    // the configuration can have either a selection object or a merge
    // list, not both
    Opts association(json.get("association"));
    const nlohmann::ordered_json& merge = json.get("merge");
    if (!association.empty() && !merge.empty()) {
      throw std::runtime_error(
        "Truth config can't contain both association and merging");
    }

    std::string particles = "";
    if (!association.empty()) {
      TruthSelectorConfig tsc;
      particles = association.require<std::string>("particles");
      tsc.particle = get_truth_particle(particles);
      const auto& containers = association.get<std::vector<std::string>>("containers", {});
      if (!containers.empty()) {
        tsc.containers = containers;
      } else {
        tsc.containers = {association.get("container", "TruthParticles")};
      }
      auto& kin = tsc.kinematics;
      kin.pt_minimum = association.get("pt_minimum", 0.0f);
      kin.abs_eta_maximum = association.get("abs_eta_maximum", INFINITY);
      kin.dr_maximum = association.require<float>("dr_maximum");
      cfg.selection = tsc;
    }
    association.throw_if_unused("selection");

    if (!merge.empty()) {
      cfg.merge = merge.get<std::vector<std::string>>();
    }

    cfg.overlap_dr = json.get("overlap_dr", 0.0);

    // use association name if specified, otherwise reuse particles name
    std::string association_name = json.get("association_name", "");
    if (not association_name.empty()) {
      cfg.association_name = association_name;
    }
    else if (not particles.empty()) {
      cfg.association_name = particles;
    }
    else {
      throw std::runtime_error(
        "If not specifying \"association_name\" you need to specify some"
        " \"particles\" to associate");
    }

    Opts output(json.get("output"));
    if (!output.empty()) {
      TruthOutputConfig out;
      out.n_to_save = output.require<int>("n_to_save");
      out.sort_order = get_truth_sort_order(
        output.require<std::string>("sort_order"));
      out.name = output.get("name", cfg.association_name);
      cfg.output = out;
    }
    output.throw_if_unused("output");

    cfg.decorate = json.get("decorate_summary", false);

    json.throw_if_unused("truth config");

    return cfg;
  }

  DecorateConfig get_decoration_config(const nlohmann::ordered_json& raw) {
    namespace cft = ConfigFileTools;
    DecorateConfig cfg;

    // the decorators will default to false, but we also make sure
    // that every decorator specified in the configuration is used.
    ConfigFileTools::OptionalConfigurationObject nlocfg(raw);
#define FILL(field) cfg.field = nlocfg.get(#field, false)
    FILL(jet_aug);
    FILL(btag_jes);
    FILL(soft_muon);
    FILL(track_truth_info);
    FILL(track_sv_info);
    FILL(track_lepton_id);
    FILL(do_vrtrackjets_fix);
    FILL(do_heavyions);
    FILL(lepton_decay_label);
#undef FILL
    nlocfg.throw_if_unused("decorations");
    return cfg;
  }

  JetCalibrationConfig get_jet_calibration(
    const boost::property_tree::ptree& pt)
  {
    JetCalibrationConfig config;
#define FILL(field) config.field = pt.get<std::string>(#field)
    FILL(collection);
    FILL(configuration);
    FILL(seq);
    FILL(area);
#undef FILL
    return config;
  }

  SelectionConfig get_selection_config(const nlohmann::ordered_json& raw) {
    SelectionConfig cfg;
    ConfigFileTools::OptionalConfigurationObject nlocfg(raw);
    // make sure the type of `defval` matches the type of `field`
#define FILL(field, defval) cfg.field = nlocfg.get(#field, defval)
    FILL(truth_jet_matching, false);
    FILL(truth_primary_vertex_matching, false);
    FILL(minimum_jet_constituents, 0);
    FILL(minimum_jet_pt, 0.0);
    FILL(maximum_jet_absolute_eta, INFINITY);
    FILL(minimum_jvt, -INFINITY);
#undef FILL
    // jet cleaning
    std::string jet_cleaning = nlocfg.get("jet_cleaning","none");
    const std::map<std::string, JetCleanOption> jetclean_option_map = {
      {"none", JetCleanOption::none},
      {"event", JetCleanOption::event},
      {"jet", JetCleanOption::jet} };
    if (!jetclean_option_map.count(jet_cleaning)) {
      std::string problem = "unknown jet cleaning '" + jet_cleaning +
        "', pick one of the following";
      std::string sep = ": ";
      for (const auto& itr: jetclean_option_map) {
        problem.append(sep + "'" + itr.first + "'");
        sep = ", ";
      }
      throw std::runtime_error(problem);
    }
    cfg.jet_cleaning = jetclean_option_map.at(jet_cleaning);

    nlocfg.throw_if_unused("selection options");
    return cfg;
  }

  SubjetConfig get_subjet_config(const boost::property_tree::ptree &subjet) {
    namespace cft = ConfigFileTools;
    SubjetConfig cfg;
    cfg.input_name = subjet.get<std::string>("input_name");
    cfg.output_name = subjet.get<std::string>("output_name");
    cfg.n_subjets_to_save = subjet.get<size_t>("n_subjets_to_save");
    cfg.min_jet_pt = subjet.get<double>("min_jet_pt");
    cfg.max_abs_eta = subjet.get<double>("max_abs_eta");
    cfg.num_const = subjet.get<size_t>("num_const");
    cfg.btagging_link = subjet.get<std::string>("btagging_link");
    // read in the b-tagging variables
    cfg.variables = cft::get_variable_list(subjet.get_child("variables"));
    return cfg;
  }

  // do some consistency checks
  void requireBTag(bool variable, const std::string& varname,
                   const std::string& btagging_link) {
    if (variable && btagging_link.empty()) {
      throw std::runtime_error(varname + " requires btagging_link to be set");
    }
  }
  void checkConsistency(const SingleBTagConfig& cfg) {
    const std::string& btagname = cfg.btagging_link;
    const DecorateConfig& dec = cfg.decorate;
#define REQUIRES_BTAG(name) requireBTag(dec.name, #name, btagname)
    REQUIRES_BTAG(btag_jes);
    REQUIRES_BTAG(soft_muon);
    REQUIRES_BTAG(jet_aug);
    #undef REQUIRES_BTAG
    const auto& dl2s = cfg.dl2_configs;
    int n_btags = std::count_if(
      dl2s.begin(), dl2s.end(),
      [](auto& x) { return x.where == DL2Config::Where::BTAG; });
    requireBTag(n_btags > 0, "applying btags", btagname);
  }

  DL2Config get_dl2_config(const nlohmann::ordered_json& node) {

    // keeps track of used keys
    ConfigFileTools::OptionalConfigurationObject nlocfg(node);

    using Where = DL2Config::Where;
    auto where = nlocfg.get<Where>("where", Where::BTAG);
    if (where == Where::UNKNOWN) {
      throw std::runtime_error("unknown tagging target");
    }

    using Engine = DL2Config::Engine;
    auto engine = nlocfg.get<Engine>("engine", Engine::DL2);
    if (engine == Engine::UNKNOWN) throw std::runtime_error(
      "unknown tagging engine");

    // get the variable remapping
    const auto& remap_obj = nlocfg.get("remapping", nlohmann::json::object());
    const auto& remapping = remap_obj.get<std::map<std::string, std::string>>();

    // get the file path
    std::string nn_file_path = nlocfg.get<std::string>("nn_file_path", "");
    if (nn_file_path.empty()) throw std::runtime_error("No nn_file_path found in DL2 config");

    // get the flip tag config
    const auto& flip_tag_config = FlavorTagDiscriminants::flipTagConfigFromString(
      nlocfg.get("flip_tag_config", "STANDARD")
    );

    // check we used everything and return
    nlocfg.throw_if_unused("DL2 key");
    return {where, engine, nn_file_path, remapping, flip_tag_config};
  }

}

SingleBTagConfig get_singlebtag_config(const std::filesystem::path& cfg_path)
{
  namespace fs = std::filesystem;
  namespace cft = ConfigFileTools;
  if (!fs::exists(cfg_path)) {
    throw std::runtime_error(cfg_path.string() + " doesn't exist");
  }
  std::ifstream cfg_stream(cfg_path);
  auto nlocfg = nlohmann::ordered_json::parse(cfg_stream);
  cft::combine_files(nlocfg, cfg_path.parent_path());
  return get_singlebtag_config(nlocfg, cfg_path.stem());
}

SingleBTagConfig get_singlebtag_config(const nlohmann::ordered_json& nlocfg,
                                       const std::string& tool_prefix)
{
  namespace pt = boost::property_tree;
  namespace cft = ConfigFileTools;

  SingleBTagConfig config;
  config.tool_prefix = tool_prefix;

  config.selection = get_selection_config(nlocfg.at("selection"));

  // convert back to ptree for now, but new code should use nlocfg if
  // possible!
  auto cfg = cft::from_nlohmann(nlocfg);

  config.jet_collection = cfg.get<std::string>("jet_collection");

  const pt::ptree& jet_calib = cfg.get_child("calibration");
  if (!jet_calib.empty()) {
    config.calibration = get_jet_calibration(jet_calib);
  }

  if (config.selection.jet_cleaning == JetCleanOption::jet &&
      config.jet_collection == "AntiKt4EMPFlowJets") {
    throw std::runtime_error(
      "Must not use individual jet cleaning for " + config.jet_collection +
      ". Use {\"jet_cleaning\": \"event\"} instead.");
  }

  config.vertex_collection = cfg.get<std::string>("vertex_collection");
  config.btagging_link = cfg.get<std::string>("btagging_link");
  
  for (const auto& trkpt: cfg.get_child("tracks")) {
    config.tracks.push_back(get_track_config(trkpt.second));
  }

  config.nntc=cfg.get<std::string>("nntc","");

  // The hits block should exist for trackless studies
  if (const auto& hitpt = cfg.get_child_optional("hits")) {
    config.hits = get_hit_config(*hitpt);
  }

  for (const auto& trthpt: nlocfg.at("truths")) {
    config.truths.push_back(get_truth_config(trthpt));
  }

  // optional configuration
  for (const auto& nn_cfg: nlocfg.at("dl2_configs")) {
    config.dl2_configs.push_back(get_dl2_config(nn_cfg));
  }

  // subjet config
  auto subjetsTree = cfg.get_child_optional("subjets");
  if (subjetsTree){
    for (const auto &subnode : subjetsTree.get()) {
        config.subjet_configs.push_back(get_subjet_config(subnode.second));
    }
  }

  // pflow constituents
  if (const auto& flow = cfg.get_child_optional("flow")) {
    config.flow = get_jet_link_config(*flow);
  }

  config.btag = cft::get_variable_list(cfg.get_child("variables.btag"));
  config.default_flag_mapping = cft::check_map_from(
    cft::get_variable_list(cfg.get_child("variables.default_mapping")));

  config.decorate = get_decoration_config(nlocfg.at("decorate"));

  checkConsistency(config);

  return config;
}

void force_full_precision(JetConstituentWriterConfig::OutputBlock& block) {
  using Type = JetConstituentWriterConfig::Type;
  for (auto& output: block.outputs) {
    if (output.type == Type::HALF) output.type = Type::FLOAT;
    if (output.type == Type::CUSTOM_HALF) output.type = Type::CUSTOM;
  }
}

void force_full_precision(SingleBTagConfig& cfg) {
  cfg.force_full_precision = true;
  for (auto& trk: cfg.tracks) {
    force_full_precision(trk.writer.constituent);
    for (auto& [_, block]: trk.writer.associated) {
      force_full_precision(block);
    }
  }
}

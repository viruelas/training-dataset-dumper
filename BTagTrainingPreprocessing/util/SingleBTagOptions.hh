#ifndef SINGLE_BTAG_OPTIONS_HH
#define SINGLE_BTAG_OPTIONS_HH

#include <vector>
#include <string>
#include <filesystem>

struct SingleTagIOOpts
{
  std::vector<std::string> in;
  std::string out;
  unsigned long long max_events;
  std::filesystem::path config_file_name;
  bool force_full_precision;
};

SingleTagIOOpts get_single_tag_io_opts(int argc, char* argv[]);


#endif

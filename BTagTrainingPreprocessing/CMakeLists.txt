#
# Cmake for BTagTrainingPreprocessing
#

# Set the name of the package:
atlas_subdir( BTagTrainingPreprocessing )

# External(s) used by the package:
find_package(HDF5 1.10.1 REQUIRED COMPONENTS CXX C)
find_package(nlohmann_json REQUIRED)
find_package(Boost REQUIRED)

# We don't want any warnings in compilation
add_compile_options(-Werror)

# We have to define the libraries first because we might not use
# some. This lets us add them condionally.
set(LINK_LIBRARIES
  xAODRootAccess
  xAODCaloEvent
  xAODTracking
  xAODPFlow
  xAODJet
  xAODTruth
  xAODMuon
  xAODEgamma
  HDF5Utils
  xAODCutFlow
  AsgTools
  JetCalibToolsLib
  JetSelectorToolsLib
  InDetTrackSelectionToolLib
  FlavorTagDiscriminants
  PathResolver
  CutBookkeeperUtils
  JetMomentToolsLib
  InDetTrackSystematicsToolsLib
  xAODHIEvent
  MCTruthClassifierLib
  JetWriters
  ElectronPhotonSelectorToolsLib
  )
# anything that is built conditionally goes here
if (NOT XAOD_STANDALONE)
  list(APPEND LINK_LIBRARIES TrigDecisionToolLib)
endif()

# common requirements
atlas_add_library(dataset-dumper
  src/BTagJetWriter.cxx
  src/BTagJetWriterUtils.cxx
  src/BTagJetWriterConfig.cxx
  src/SubjetWriter.cxx
  src/FatJetWriter.cxx
  src/FatJetWriterConfig.cxx
  src/SubstructureAccessors.cxx
  src/BTagTrackWriter.cxx
  src/HitWriter.cxx
  src/JetConstWriter.cxx
  src/TruthWriter.cxx
  src/addMetadata.cxx
  src/TrackSelector.cxx
  src/TruthTools.cxx
  src/ConstituentSelector.cxx
  src/DecoratorExample.cxx
  src/JetTruthAssociator.cxx
  src/JetTruthMerger.cxx
  src/TrackTruthDecorator.cxx
  src/TrackVertexDecorator.cxx
  src/TrackLeptonDecorator.cxx
  src/ConfigFileTools.cxx
  src/BTagInputChecker.cxx
  src/TruthCorruptionCounter.cxx
  src/errorLogger.cxx
  src/streamers.cxx
  src/trackSort.cxx
  src/cleanHits.cxx
  src/processSingleBTagEvent.cxx
  src/HitDecorator.cxx
  src/BJetShallowCopier.cxx
  src/SingleBTagConfig.cxx
  src/SingleBTagTools.cxx
  src/JetLeptonDecayLabelDecorator.cxx
  src/LeptonTruthDecorator.cxx
  src/JetTruthSummaryDecorator.cxx
  PUBLIC_HEADERS src
  LINK_LIBRARIES ${LINK_LIBRARIES}
  )

# Build the executables
atlas_add_executable( dump-single-btag
  util/dump-single-btag.cxx
  util/SingleBTagOptions.cxx
  )
target_link_libraries( dump-single-btag dataset-dumper)
target_include_directories( dump-single-btag PRIVATE src)

atlas_add_executable( test-config-merge
  util/test-config-merge.cxx
  src/ConfigFileTools.cxx
  INCLUDE_DIRS
  ${Boost_INCLUDE_DIRS}
  LINK_LIBRARIES
  ${Boost_LIBRARIES}
  -lstdc++fs nlohmann_json::nlohmann_json
  )
atlas_add_executable( test-config-parse-single-b
  util/test-config-parse-single-b.cxx
  LINK_LIBRARIES dataset-dumper)

# we only build the algorithm stuff for Gaudi builds
if (NOT XAOD_STANDALONE)
  atlas_add_component(BTagTrainingPreprocessing
    src/SingleBTagAlg.cxx
    src/TriggerJetGetterAlg.cxx
    src/TrackSystematicsAlg.cxx
    src/TriggerBTagMatcherAlg.cxx
    src/JetMatcherAlg.cxx
    src/MCTCDecoratorAlg.cxx
    src/H5FileSvc.cxx
    src/components/*.cxx
    LINK_LIBRARIES
    dataset-dumper
    AnaAlgorithmLib
    DerivationFrameworkInterfaces
    )
  atlas_install_scripts(
    bin/ca-dump-retag
    bin/ca-dump-single-btag
    bin/ca-dump-single-btag-fixedcone
    bin/ca-dump-trigger-pflow
    bin/ca-dump-trigger-emtopo
    bin/ca-dump-trigger-all
    bin/ca-dump-trigger-mc16
    bin/ca-dump-trigger-workingpoints
    bin/ca-dump-minimal-btag
    bin/ca-dump-upgrade
    bin/ca-dump-lrt
    bin/ca-dump-upgrade-HI
    bin/dump-trigger-menu
    bin/ca-make-test-file
    bin/ca-dump-multi-config
    POST_BUILD_CMD ${ATLAS_FLAKE8} --extend-ignore=ATL902
    )
  atlas_install_python_modules(
    python/trigger.py
    python/trackUtil.py
    python/retag.py
    python/dumper.py
    python/mctc.py
    POST_BUILD_CMD ${ATLAS_FLAKE8} --extend-ignore=ATL902
    )
endif()


atlas_install_scripts(
  test/test-dumper
  test/test-configs-single-b
  test/test-output
  batch/batch-single-btag
  grid/submit-single-btag
  grid/submit-trigger
  grid/submit-HI
  grid/submit-retag
  grid/grid-submit
  )


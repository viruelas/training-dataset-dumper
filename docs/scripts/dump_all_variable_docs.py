#!/usr/bin/env python3

"""
Generate documentation for all defined variables
"""
_spec_help = 'Yaml variable specification file'
_dump_header ="Comprehensive variable description\n==================================\n\n"

from argparse import ArgumentParser
from sys import stdout, stderr
from os import makedirs
from os.path import join


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('-s', '--spec-file', default=join('docs', 'data', 'field-descriptions.yaml'),
                        help=_spec_help)
    parser.add_argument('-d', '--output_dir', default='ci-docs-all')
    parser.add_argument('-o', '--output_file', default='vars_all.md')
    return parser.parse_args()


def run():
    args = get_args()

    import yaml
    from make_variable_docs import write_markdown

    with open(args.spec_file) as spec_file:
        fields = yaml.safe_load(spec_file)

    makedirs(args.output_dir, exist_ok=True)
    out_file = open(join(args.output_dir, args.output_file), 'w')

    out_file.write(_dump_header)
    write_markdown(fields, out_file)
                

if __name__ == '__main__':
    run()

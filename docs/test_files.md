Making DAODs from AODs
======================

You can reproduce a DAOD from its source, but unfortunately there are
a few steps which you have to follow carefully. Suppose you'd like a
10 event version of
`DAOD_PHYSVAL.410470.e6337_s3681_r13144_p5057.pool.root`. You'll need
to find:

1. The input AOD file, and
2. The command to produce the DAOD from the AOD

And then use this information to build a new file.

Finding the input AOD with rucio
--------------------------------

You can dig up the input file with some detective work and rucio. Running

```
setupATLAS
lsetup rucio
```

will set up rucio. You can then search with `rucio ls`, using the
wildcard `*` to fill in unknown parts of the dataset:

```
rucio ls 'mc20_13TeV:mc20_13TeV*.410470.*e6337_s3681_r13144_p5057*'
```

will return several datasets (note you have to guess the "scope", it's
usually either `mc20_13TeV:` or `mc16_13TeV:`). The above gives one
dataset called

```
mc20_13TeV:mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYSVAL.e6337_s3681_r13144_p5057
```

It probably came from an input with a similar name, without the
`p5057`. Let's try

```
rucio ls 'mc20_13TeV:mc20_13TeV*.410470.*e6337_s3681_r13144/'
```

note the `/` at the end indicating the end of the container name. That
returns a file named

```
mc20_13TeV:mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_s3681_r13144
```

Download one of them with `rucio get --nrandom 1 <the file>`.


Finding the command to reproduce the derivation
-----------------------------------------------

It's important that you set up the same release and produce the
derivation with the same parameters that it was initially built
with. You can look up the [`p5057` tag with ami][ami]. This will show
you that the `cacheName` was `22.0.62`.

You'll need to open a clean shell and set up this release

```
setupATLAS
asetup Athena,22.0.62
```

and then produce a new DAOD with

```
Reco_tf.py --AMIConfig p5057 --maxEvents 10 --inputAODFile AOD.pool.root --outputDAODFile small.pool.root --reductionConf PHYSVAL
```

Where `AOD.pool.root` is the name of the AOD you just
downloaded. There are several additional arguments:

- `--AMIConfig p5057` will ensure that the transform is run with
  the same settings that produced your original DAOD.

- `--maxEvents 10` tells the job to stop after 10 events.

- `--outputDAODFile small.pool.root` specifies the output type as DAOD
  and gives a file extension.

- `--reductionConf PHYSVAL` specifies that the output is in the
  `PHYSVAL` format.

This should produce a file called `DAOD_PHYSVAL.small.pool.root`. You can check the contents with

```
checkFile DAOD_PHYSVAL.small.pool.root
```

You can cross check this with the officially produced samples to make
sure they contain the same information.


Uploading test files
--------------------

See the instructions [here](development.md#uploading-test-files).



[ami]: https://ami.in2p3.fr/?subapp=tagsShow&userdata=p5057

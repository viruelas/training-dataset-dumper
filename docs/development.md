### Contributing guidelines

If you want to contribute to the development of the training dataset dumper, or push tags using the grid submit script, you should first create a fork of the repository.
You can read in detail about the git forking workflow [here](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow). Gitlab also provides documentation on how to create a fork [here](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html).
After forking the [main repository]({{repo_url}}), you can clone your fork and set the upstream url.

```bash
git clone ssh://git@gitlab.cern.ch:7999/<cern_username>/training-dataset-dumper.git
git remote add upstream ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
```

You can use the fork to keep track of your changes and if you find them well-placed to be added to the `r22` branch, you can do so via a merge request.
In order to integrate changes to your target branch that may have been merged during the development of your changes, you may have to [rebase](https://docs.gitlab.com/ee/topics/git/git_rebase.html#git-rebase) your development branch from upstream. More information about rebasing can be found [here](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase).

#### Adding Features / Fixing Bugs

Please note that in the interest of keeping this package from growing too large,
some features (in particular those not directly related to the dumping of information
from xAOD) may have a better home elsewhere.
The best place to implement a feature can be discussed on mattermost on in an issue.

In any case, the following procedure should be roughly followed:

1. (Optional) **Informal discussion** on [mattermost](https://mattermost.web.cern.ch/aft-algs/channels/h5-dumper) to check if the feature/bug exists, and is a suitable addition.
2. **Open an issue** on [GitLab]({{repo_url}}-/issues). This is a place to describe in more detail the feature/bug, and to work out what is necessary to change in the code.
3. **Assign a responsible person** to the issue. If the person who opened the issue has they capability, by default they should assign themselves to the issue. Otherwise the maintainers will assign a responsible person.
4. **Merge request**. The assigned person should work on the feature and open a MR. This will be reviewed, any follow-up issues created, and finally merged. The corresponding issue should be closed.

### Package Layout

The code lives under [`BTagTrainingPreprocessing`]({{repo_url}}-/tree/r22/BTagTrainingPreprocessing). All the top-level
executables live in [`util/`]({{repo_url}}-/tree/r22/BTagTrainingPreprocessing/util), whereas various private internal classes
are defined in [`src/`]({{repo_url}}-/tree/r22/BTagTrainingPreprocessing/src).

### Adding More Outputs

There are two general steps for data flow in this package:

- **Augmenters** manipulate xAOD objects: they read in objects, calculate any properties of interest, and store (decorate) these properties on the same objects.
- **Writers** are responsible for xAOD -> HDF5 transcription: they read xAOD objects and write the associated data to HDF5 files.

We enforce this separation so that augmenters can easily be ported upstream to derivations, reconstruction, or the trigger. It also helps to keep the xAOD -> HDF5 transcription generic.

#### Adding an augmentation

There's an example class in [`src/DecoratorExample.cxx`]({{repo_url}}-/blob/r22/BTagTrainingPreprocessing/src/DecoratorExample.cxx) which should
make the implementation a bit more clear. This tool runs in the standard dumping script, so you can get a better picture of how it's implemented [by searching for it][de].

[de]: https://gitlab.cern.ch/search?search=DecoratorExample&group_id=7674&project_id=34016&scope=&search_code=true&snippets=false&repository_ref=r22

It should be easy to write out any simple (primitive type) decoration you add to a jet, the `BTagging` object, the `EventInfo` object, or a track.

#### Writing out an augmentation

The writer classes, like most code in this package, are configured via a json file.

Each configuration file has an object called
`"variables"`, which specifies the per-jet outputs. These are also specified
by type: there is one list for `"floats"`, one for `"chars"`,
etc. We assume that the variables are stored on the `BTagging` object by default, but there are also `"jet_int_variables"` and `"jet_floats"` for
anything on the jet itself. An `"event"` field specifies information that should be read off the `EventInfo` object.

Track-wise variables are specified within a similar structure within the `"tracks"` field.

Nothing is saved to output files by default: you need to add whatever
you've decorated to the b-tagging object to the output list. If the
output isn't found on the xAOD, the code should immediately throw an
exception.


### Editing Athena packages

You can modify existing Athena packages by building them locally
alongside the code in this package. You'll need to check out a local
copy of the `athena` repository. You should do this in the root
directory of the package source, i.e. alongside `docs/`, `configs`,
`README.md`, etc.

You can use [`git-fatlas`][fatlas]:

```bash
git-fatlas-init -r master
git-fatlas-add path/to/package
```

or use `git atlas` which should be accessible via `lsetup git`. Note that you might have to manually delete the `Projects` directory if you rely on `git atlas`.


### Adding Tests

If you're developing an application that needs a specific
configuration file, input format, or top-level
`ComponentAccumulator`-based script, we encourage you to write
continuous integration tests so that we won't break your workflow.

The main test script lives in
[`test/test-dumper`]({{repo_url}}-/tree/r22/BTagTrainingPreprocessing/test/test-dumper),
and is invoked by running `test-dumper <mode>`, where `<mode>` is a
key that selects the configuration file, the input data file, and the
top level test executable to run the dumper. To add a new mode it
should be sufficient to edit `test-dumper` and add a new entry under
`CONFIGS`, `DATAFILES`, and `TESTS`.

To add the tests to CI, you'll also have to edit
[`.gitlab-ci.yml`]({{repo_url}}-/tree/r22/.gitlab-ci.yml)

#### Making inputs for tests

We keep the input files small (roughly 10 MB) so that tests are quick
to run locally. If you need a new input format for your test, you can
create a smaller file in several ways.

Sometimes you can download a dataset from the grid, set up up a 
Gaudi-based release, and run

```bash
acmd merge-files <xAOD-file> -o test.pool.root --evts 10
```

Unfortunately this often fails for DAODs (see
[ATLASRECTS-7082][rects-7082]). In this case, see the section [Making
DAODs from inputs](test_files.md).

#### Uploading test files

Test files are stored in a [dedicated repository][dumper-test-files]
via [Git LFS][lfs]. After you've confirmed that you can use your 
reduced test file, create a MR to this repository including your file.
Be sure to commit your file using Git LFS.

??? info "Using Git LFS"

    Using Git LFS requires LFS to be installed. Several installation 
    options are listed [here][lfs-install]. If you use conda you can 
    also run simply run `conda install git-lfs`.

    However you install Git LFS, you need to run the setup command
    once per user account.

    ```bash
    git lfs install
    ```

    Once run, Git will automatically commit any files named `*.root` 
    using LFS.

### Editing the documentation

The documentation is provided by [mkdocs][mkd], and deployed via CERN Gitlab. For any larger edits to the documentation we recommend running mkdocs locally. It can be installed with pip:

```bash
pip install -r docs/requirements.txt
```

and then launched from the root directory of this project

```bash
mkdocs serve
```

This will launch a server and provide you with a local URL to view the pages.

[fatlas]: https://github.com/dguest/git-fatlas
[mkd]: https://www.mkdocs.org/
[rects-7082]: https://its.cern.ch/jira/browse/ATLASRECTS-7082
[lfs]: https://git-lfs.github.com/
[lfs-install]: https://www.atlassian.com/git/tutorials/git-lfs#installing-git-lfs
[dumper-test-files]: https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/dumper-test-files